<?php
require_once "Conexao/operadores.php";

$operadores     = new operadores();
$cinemas        = $operadores->allCinemas();
$filmes         = $operadores->listarFilmes();
$salas          = $operadores->listarSala();
$sessoes        = $operadores->listarSessao();
?>

<head>
    <meta charset="UTF-8">
    <title>Cadeira</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>

<?php include "header.php"; ?>

<div class="conteudo">
    <div class="center font2">Adicionar uma Sessão</div>
    <form name="form_sessao" id="sessao" method="post" action="Controller/Adicionar_Sessao.php" class="font1">
        Cinema:
        <select id="cinema" name="cinema">
            <?php foreach ($cinemas as $cinema) { ?>
                <option value="<?= $cinema["idCinema"]; ?>"> <?= $cinema["nome"]; ?></option>
            <?php   }  ?>
        </select><br>
        Filme:
        <select id="filme" name="filme">
            <?php foreach ($filmes as $filme) { ?>
                <option value="<?= $filme["idFilmes"]; ?>"> <?= $filme["nome"]; ?></option>
            <?php   }  ?>
        </select><br>
        Numero da sala:
        <select id="sala" name="sala">
            <?php foreach ($salas as $sala) { ?>
                <option value="<?= $sala["idSala"]; ?>"> <?= $sala["numero"]; ?></option>
            <?php   }  ?>
        </select><br>
        Data:
        <input type="date" id="data" name="data"><br>
        Duracao:
        <input type="number" id="duracao" name="duracao"><br>
        <input type="submit" value="cadastrar">
    </form>


<div class="lista">
    <br><br>

    <div class="center font2">Lista de Sessoes</div>
    <div style="overflow: auto; width: 640px; height: 200px; border:solid 1px">
        <table class="tabela" id="sql" style="width:800px; text-align:center;">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Cinema</th>
                    <th>Filme</th>
                    <th>Número da Sala</th>
                    <th>Data</th>
                    <th>Duracao</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($sessoes as $sessao) { ?>
                    <tr>
                        <td><?= $sessao["idSessao"] ?></td>
                        <td><?= $sessao["cinema"] ?></td>
                        <td><?= $sessao["filme"] ?></td>
                        <td><?= $sessao["numero"] ?></td>
                        <td><?= $sessao["data"] ?></td>
                        <td><?= $sessao["duracao"] ?></td>
                        <td>
                            <a href="EditarSessao.php?id=<?=$sessao["idSessao"]?>">Editar</a>
                            <a href="Controller/Excluir_Sessao.php?id=<?=$sessao["idSessao"]?>">Excluir</a>
                        </td>
                    </tr>
                <?php   }  ?>
            </tbody>
        </table>
    </div>

    <br><br>
</div>

</div>