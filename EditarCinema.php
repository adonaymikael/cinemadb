<?php
require_once "Conexao/operadores.php";
$id = $_GET['id'];

$operadores = new operadores();
$cinema     = $operadores->cinemaByID($id);
$enderecos  = $operadores->AllEndereco();
?>

<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <title>Mapa Sala</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>


<div class="conteudo font3">
    <div class="center font2">Editar Cinema</div>
    <form name="editar_cinema" id="editar_cinema" method="post" action="Controller/Editar_Cinema.php" class="font1">
        Id:
        <input readonly="true" type="text" id="id" class="key" name="id" value="<?= $cinema["idCinema"] ?>"><br>
        Nome do Cinema:
        <input  type="text" id="nome" name="nome" value="<?= $cinema["nome"] ?>"><br> <!-- Chave Primaria não pode alterar -->
        Cnpj:
        <input  type="text" id="cnpj" name="cnpj" value="<?= $cinema["cnpj"] ?>"><br> <!-- Chave Primaria não pode alterar -->
        Endereço:
        <select id="end_cinema" name="end_cinema">
            <?php foreach ($enderecos as $endereco) { ?>
                <option value="<?= $endereco["idEndereco"]; ?>"> <?= $endereco["logradouro"]," ". $endereco["cidade"]; ?></option>
            <?php   }  ?>
        </select><br>
        <input type="submit" value="Alterar">
        <a href="cinema.php"><input type="button" value="Voltar"></a>
    </form>
</div>