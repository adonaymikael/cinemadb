<?php
require_once "Conexao/operadores.php";

$operadores = new operadores();
$salas = $operadores->AllMapa_Salas();
$sala_reserva = $operadores->AllCadeira_cadeiraReserva();
$ListarIdSala = $operadores->ListarIdSala();
$ListarCadeiraFileira = $operadores->ListarCadeiraFileira();
$ListarCadeiraNumero = $operadores->ListarCadeiraNumero();
?>

<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <title>Mapa Sala</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>

<body>
    <?php include "header.php"; ?>



    <div class="conteudo">

        <div class="center2 font2">Adicionar novo Mapa de Sala</div>
        <form name="Fromsala" id="Fromsala" method="post" action="Controller/Adicionar_Mapa-Sala.php" class="font1">
            ID Sala:
            <select id="sala_idSala" name="sala_idSala">
                <?php foreach ($ListarIdSala as $idsala) { ?>
                    <option value="<?= $idsala["idSala"]; ?>"><?= $idsala["idSala"]; ?></option>
                <?php   }  ?>
            </select><br>
            Cadeira Fileira:
            <select id="cadeira_fileira" name="cadeira_fileira">
                <?php foreach ($ListarCadeiraFileira as $CadeiraFileira) { ?>
                    <option value="<?= $CadeiraFileira["fileira"]; ?>"> <?= $CadeiraFileira["fileira"]; ?></option>
                <?php   }  ?>
            </select><br>
            Cadeira Numero:
            <select id="cadeira_numero" name="cadeira_numero">
                <?php foreach ($ListarCadeiraNumero as $CadeiraNumero) { ?>
                    <option value="<?= $CadeiraNumero["numero"]; ?>"> <?= $CadeiraNumero["numero"]; ?></option>
                <?php   }  ?>
            </select><br>
            <input type="submit" value="cadastrar">
        </form>

        <br><br>

        <div class="center2 font2">Lista dos Mapas de Salas</div>
        <div style="overflow: auto; width: 640px; height: 200px; border:solid 1px">
            <table class="tabela" id="sql" style="width:800px">
                <thead>
                    <tr>
                        <th>ID Sala</th>
                        <th>Cadeira Fileira</th>
                        <th>Cadeira Numero</th>
                        <th>Ação</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($salas as $c) { ?>
                        <tr>
                            <td><?= $c["sala_idSala"] ?></td>
                            <td><?= $c["cadeira_fileira"] ?></td>
                            <td><?= $c["cadeira_numero"] ?></td>
                            <td>
                                <a href="EditarMapa_Sala.php?valor1=<?= $c["sala_idSala"] ?>&valor2=<?= $c["cadeira_numero"] ?>">Editar</a>
                                <a href="Controller/Excluir_Mapa-Sala.php?valor1=<?= $c["sala_idSala"] ?>&valor2=<?= $c["cadeira_numero"] ?>">Excluir</a>
                            </td>
                        </tr>
                    <?php   }  ?>
                </tbody>
            </table>
        </div>

        <br><br>

</body>

</html>