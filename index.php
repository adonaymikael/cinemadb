<?php
require_once "Conexao/operadores.php";

$operadores = new operadores();
$cadeiras = $operadores->AllCadeira();
$cadeira_reserva = $operadores->AllCadeira_cadeiraReserva();
?>

<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <title>Cadeira</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>

<body>
    <?php include "header.php"; ?>



    <div class="conteudo">

        <div class="center font2">Adicionar nova Cadeira</div>
        <form name="FromCadeira" id="FromCadeira" method="post" action="Controller/Adicionar_Cadeira.php" class="font1">
            Fileira:
            <input type="text" id="fileira" name="fileira"><br>
            Numero:
            <input type="text" id="numero" name="numero"><br>
            Status:
            <input type="text" id="status" name="status"><br>
            Tipo:
            <input type="text" id="tipo" name="tipo"><br>
            <input type="submit" value="cadastrar">
        </form>

        <br><br>

        <div class="center font2">Lista de Cadeiras</div>
        <div style="overflow: auto; width: 640px; height: 200px; border:solid 1px">
            <table class="tabela" id="sql" style="width:800px">
                <thead>
                    <tr>
                        <th>Fileira</th>
                        <th>Numero</th>
                        <th>Status</th>
                        <th>Tipo</th>
                        <th>Ação</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($cadeiras as $c) { ?>
                        <tr>
                            <td><?= $c["fileira"] ?></td>
                            <td><?= $c["numero"] ?></td>
                            <td><?= $c["status"] ?></td>
                            <td><?= $c["tipo"] ?></td>
                            <td>
                                <a href="EditarCadeira.php?valor1=<?= $c["numero"] ?>&valor2=<?= $c["fileira"] ?>">Editar</a>
                                <a href="Controller/Excluir_Cadeira.php?valor1=<?= $c["numero"] ?>&valor2=<?= $c["fileira"] ?>">Excluir</a>
                            </td>
                        </tr>
                    <?php   }  ?>
                </tbody>
            </table>
        </div>

        <br><br>

        <div class="center3 font2">Lista de Cadeiras E suas Reservas (View com join)</div>
        <div style="overflow: auto; width: 640px; height: 200px; border:solid 1px">
            <table class="tabela" id="sql" style="width:800px">
                <thead>
                    <tr>
                        <th>Fileira</th>
                        <th>Numero</th>
                        <th>Status</th>
                        <th>Tipo</th>
                        <th>Reserva IdReserva</th>
                        <th>Cadeira Fileira</th>
                        <th>Cadeira Numero</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($cadeira_reserva as $c) { ?>
                        <tr>
                            <td><?= $c["fileira"] ?></td>
                            <td><?= $c["numero"] ?></td>
                            <td><?= $c["status"] ?></td>
                            <td><?= $c["tipo"] ?></td>
                            <td><?= $c["reserva_idReserva"] ?></td>
                            <td><?= $c["cadeira_fileira"] ?></td>
                            <td><?= $c["cadeira_numero"] ?></td>
                        </tr>
                    <?php   }  ?>
                </tbody>
            </table>
        </div>

        <br><br>

</body>

</html>
