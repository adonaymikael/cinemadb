<?php
require_once "Conexao/operadores.php";
$operadores = new operadores();

$idtipo_pagamento = $_GET['idtipo_pagamento'];
$allPaymentTypes = $operadores->paymentTypeById($idtipo_pagamento);
?>

<head>
    <meta charset="UTF-8">
    <title>Cadeira</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>

<div class="conteudo font1">
    <h3>Editar Tipo de Pagamento</h3>
    <form method="post" action="Controller/Editar_Tipo_Pagamento.php">
        Id:
        <input type="text" readonly="true" class="key" value="<?= $allPaymentTypes["idtipo_pagamento"] ?>" name="idtipo_pagamento" id="idtipo_pagamento"><br>

        Descrição:
        <input type="text" value="<?= $allPaymentTypes["descricao"] ?>" name="descricao" id="descricao"><br><br>
        <input type="submit" value="Editar">
        <a href="usuario.php"><input type="button" value="Voltar"></a>
    </form>
</div>


