<?php
require_once "Conexao/operadores.php";

$operadores     = new operadores();
$allusers      = $operadores->getAllUsers();
?>

<head>
    <meta charset="UTF-8">
    <title>Cadeira</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>

<?php include "header.php"; ?>

<div class="conteudo">
    <h3>Adicionar usuário</h3>
    <form method="post" action="Controller/Adicionar_Usuario.php">
        Nome do usuário:
        <input type="text" name="nome" id="nome"><br><br>
        CPF:
        <input type="text" name="cpf" id="cpf"><br><br>
        Senha:
        <input type="password" name="pass" id="pass"><br><br>
        Sexo: <br>
        <input type="radio" name="genero" value="Masculino">Masculino<br>
        <input type="radio" name="genero" value="Feminino">Feminino<br><br>
        Idade:
        <input type="text" name="idade" id="idade"><br><br>
        E-mail:
        <input type="text" name="email" id="email"><br><br>

        <input type="submit" value="Cadastrar">
    </form>

<div class="center2 font2">Lista de Usuários</div>
        <div style="overflow: auto; width: 640px; height: 200px; border:solid 1px">
            <table class="tabela" id="sql" style="width:800px; text-align:center;">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Nome</th>
                        <th>CPF</th>
                        <th>Senha</th>
                        <th>Sexo</th>
                        <th>Idade</th>
                        <th>E-mail</th>
                        <th>Ação</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($allusers as $user) { ?>
                        <tr>
                            <td><?= $user["idusuario"] ?></td>
                            <td><?= $user["nome"] ?></td>
                            <td><?= $user["cpf"] ?></td>
                            <td><?= $user["senha"] ?></td>
                            <td><?= $user["sexo"] ?></td>
                            <td><?= $user["idade"] ?></td>
                            <td><?= $user["email"] ?></td>
                            <td>
                                <a href="EditarUsuario.php?idusuario=<?= $user["idusuario"] ?>">Editar</a>
                                <a href="Controller/Excluir_Usuario.php?idusuario=<?= $user["idusuario"] ?>">Excluir</a>
                            </td>
                        </tr>
                    <?php   }  ?>
                </tbody>
            </table>
        </div>

<!-- 
<div class="conteudo">
    <div class="center font2">Adicionar um Cinema</div>
    <form name="form_cinema" id="cinema" method="post" action="Controller/Adicionar_Cinema.php" class="font1">
        Nome do Cinema:
        <input type="text" id="nome" name="nome"><br>
        Cnpj:
        <input type="text" id="cnpj" name="cnpj"><br>
        Endereço:
        <select id="end_cinema" name="end_cinema">
            <?php foreach ($enderecos as $endereco) { ?>
                <option value="<?= $endereco["idEndereco"]; ?>"> <?= $endereco["logradouro"]," ". $endereco["cidade"]; ?></option>
            <?php   }  ?>
        </select><br>
        <input type="submit" value="cadastrar">
    </form>
</div>

<div class="lista">
    <br><br>

    <div class="center font2">Lista de Cadeiras</div>
    <div style="overflow: auto; width: 640px; height: 200px; border:solid 1px">
        <table class="tabela" id="sql" style="width:800px">
            <thead>
                <tr>
                    <th>id</th>
                    <th>Cinema</th>
                    <th>Cnpj</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($cinemas as $cinema) { ?>
                    <tr>
                        <td><?= $cinema["idCinema"] ?></td>
                        <td><?= $cinema["nome"] ?></td>
                        <td><?= $cinema["cnpj"] ?></td>
                        <td>
                            <a href="EditarCinema.php?id=<?=$cinema["idCinema"]?>">Editar</a>
                            <a href="Controller/Excluir_Cinema.php?id=<?=$cinema["idCinema"]?>">Excluir</a>
                        </td>
                    </tr>
                <?php   }  ?>
            </tbody>
        </table>
    </div>

    <br><br>
</div> -->

</div>