<?php
require_once "Conexao/operadores.php";
$valor1 = $_GET['valor1'];
$valor2 = $_GET['valor2'];
$operadores = new operadores();
$cadeiras = $operadores->CadeiraByID($valor1, $valor2);
?>
<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <title>Mapa Sala</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>

<body>

    <div class="conteudo font3">
        <div class="center font2">Editar Cadeira</div>
        <form name="FromCadeira" id="FromCadeira" method="post" action="Controller/Editar_Cadeira.php" class="font1">

            Fileira:
            <input readonly="true" type="text" id="fileira" class="key" name="fileira" value="<?= $cadeiras["fileira"] ?>"><br> <!-- Chave Primaria não pode alterar -->
            Numero:
            <input readonly="true" type="text" id="numero" class="key" name="numero" value="<?= $cadeiras["numero"] ?>"><br> <!-- Chave Primaria não pode alterar -->
            Status:
            <input type="text" id="status" name="status" value="<?= $cadeiras["status"] ?>"><br>
            Tipo:
            <input type="text" id="tipo" name="tipo" value="<?= $cadeiras["tipo"] ?>"><br><br>
            <input type="submit" value="Alterar">
            <a href="index.php"><input type="button" value="Voltar"></a>
        </form>
    </div>

</body>

</html>