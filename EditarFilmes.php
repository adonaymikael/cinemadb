<?php
require_once "Conexao/lucasDb.php";
$valor1 = $_GET['valor1'];
$lucasDb = new lucasDb();
$filme = $lucasDb ->FilmesByID($valor1);
$AllGenero = $lucasDb -> AllGenero();
$AllSituacao = $lucasDb -> AllSituacao();
?>
<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <title>Filme</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>

<body>

    <div class="conteudo font3">
        <div class="center font2">Editar Filme</div>
        <form name="Fromfilmes" id="Fromfilmes" method="post" action="Controller/Editar_Filmes.php" class="font1">

            <br>
            ID:
            <input type="text" readonly="true" id="idfilmes" class="key" name="idfilmes" value="<?= $valor1 ?>"><br>

            Nome:
            <input type="text" id="nome" name="nome" value="<?= $filme["nome"] ?>">
            <br>
            Classificação:
            <input type="text" id="classificacao" name="classificacao" value="<?= $filme["classificacao"] ?>">
            <br>
            Duração:
            <input type="number" id="duracao" name="duracao" value="<?= $filme["duracao"] ?>">
            <br>
            Descrição:
            <input type="text" id="descricao" name="descricao" value="<?= $filme["descricao"] ?>">
            <br>
            Tipo:
            <input type="text" id="tipo" name="tipo" value="<?= $filme["tipo"] ?>">
            <br>
            Gênero:
            <select id="idgenero" name="idgenero">
                <?php foreach ($AllGenero as $genero) { ?>
                    <option value="<?= $genero["idgenero"]; ?>"><?= $genero["descricao"]; ?></option>
                <?php   }  ?>
            </select><br>
            Situação:
            <select id="idsituacao" name="idsituacao">
                <?php foreach ($AllSituacao as $situacao) { ?>
                    <option value="<?= $situacao["idsituacao"]; ?>"><?= $situacao["descricao"]; ?></option>
                <?php   }  ?>
            </select><br><br>
        
            <input type="submit" value="alterar">
            <a href="filmes.php"><input type="button" value="Voltar"></a>
        </form>
    </div>
</body>

</html>