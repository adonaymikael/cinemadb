<?php
require_once "Conexao/operadores.php";
$id = $_GET['id'];

$operadores = new operadores();
$sessao     = $operadores->sessaoByID($id);
$filmes     = $operadores->AllFilmes();
$salas      = $operadores->AllSalas();
$cinemas    = $operadores->AllCinemas();
?>

<head>
    <meta charset="UTF-8">
    <title>Mapa Sala</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>

<div class="conteudo font3">
    <div class="center font2">Editar Sessão</div>
    <form name="editar_sessao" id="editar_sessao" method="post" action="Controller/Editar_Sessao.php" class="font1">
        Id:
        <input readonly="true" type="text" id="id" class="key" name="id" value="<?= $sessao["idSessao"] ?>"><br>
        Cinema:
        <select id="cinema" name="cinema">
            <?php foreach ($cinemas as $cinema) { ?>
                <option value="<?= $cinema["idCinema"]; ?>"> <?= $cinema["nome"]; ?></option>
            <?php   }  ?>
        </select><br>        
        Filme:
        <select id="filme" name="filme">
            <?php foreach ($filmes as $filme) { ?>
                <option value="<?= $filme["idFilmes"]; ?>"> <?= $filme["nome"]; ?></option>
            <?php   }  ?>
        </select><br>    
        Número da Sala:    
        <select id="sala" name="sala">
            <?php foreach ($salas as $sala) { ?>
                <option value="<?= $sala["idSala"]; ?>"> <?= $sala["numero"]; ?></option>
            <?php   }  ?>
        </select><br>
        Data:
        <input type="date" id="data" name="data"><?= $sessao["data"] ?><br> 
        Duração:       
        <input type="number" id="duracao" name="duracao" value="<?= $sessao["duracao"] ?>"><br>
        
        <input type="submit" value="Alterar">
        <a href="sessao.php"><input type="button" value="Voltar"></a>
    </form>
</div>