<?php
require_once "Conexao/operadores.php";
$operadores = new operadores();

$idusuario = $_GET['idusuario'];
$allUserData = $operadores->userById($idusuario);
?>

<head>
    <meta charset="UTF-8">
    <title>Cadeira</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>

<div class="conteudo font1">
    <h3>Editar usuário</h3>
    <form method="post" action="Controller/Editar_Usuario.php">
        Id:
        <input type="text" readonly="true" class="key" value="<?= $allUserData["idusuario"] ?>" name="idusuario" id="idusuario"><br>

        Nome do usuário:
        <input type="text" value="<?= $allUserData["nome"] ?>" name="nome" id="nome"><br>
        CPF:
        <input type="text" name="cpf" value="<?= $allUserData["cpf"] ?>"  id="cpf"><br>
        Senha:
        <input type="password" name="pass" value="<?= $allUserData["senha"] ?>"  id="pass"><br>
        Sexo: <br>
        <?php switch($allUserData["sexo"]) { 
                case "Masculino":
            ?>  
            <input type="radio" name="genero" value="Masculino" checked>Masculino<br>
            <input type="radio" name="genero" value="Feminino">Feminino<br>
            <?php break;
                case "Feminino": 
            ?>
            <input type="radio" name="genero" value="Masculino">Masculino<br>
            <input type="radio" name="genero" value="Feminino" checked>Feminino<br>
            <?php break; 
                default:
            ?> 
                <input type="radio" name="genero" value="Masculino">Masculino<br>
                <input type="radio" name="genero" value="Feminino">Feminino<br>
            <?php
            }
            ?>
        
        Idade:
        <input type="text" name="idade" value="<?= $allUserData["idade"] ?>"  id="idade"><br>
        E-mail:
        <input type="text" name="email" value="<?= $allUserData["email"] ?>"  id="email"><br><br>

        <input type="submit" value="Editar">
        <a href="usuario.php"><input type="button" value="Voltar"></a>
    </form>
</div>


