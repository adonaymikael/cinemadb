<?php 
header('Content-type: text/html; charset=UTF-8');
?>
<table class="centerText" id="link" style="width:400px; float:left;">
    <thead>
        <tr>
            <th>Adonay Mikael</th>
            <th>Lucas Lima</th>
            <th>Pedro Henrique</th>
            <th>Carlos Eduardo</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td><a href="index.php"><button class="btn btn-info btn-sm">Cadeira</button></a></td>
            <td><a href="filmes.php"><button class="btn btn-info btn-sm">Filmes</button></a></td>
            <td><a href="cinema.php"><button class="btn btn-info btn-sm">Cinema</button></a></td>
            <td><a href="usuario.php"><button class="btn btn-info btn-sm">Usuário</button></a></td>
        </tr>
        <tr>
            <td><a href="mapa_sala.php"><button class="btn btn-info btn-sm">Mapa Sala</button></a></td>
            <td><a href="genero.php"><button class="btn btn-info btn-sm">Gênero</button></a></td>
            <td><a href="sessao.php"><button class="btn btn-info btn-sm">Sessão</button></a></td>
            <td><a href="tipo_pagamento.php"><button class="btn btn-info btn-sm">Tipo de Pagamento</button></a></td>
        </tr>
    </tbody>
</table>