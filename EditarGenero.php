<?php
require_once "Conexao/lucasDb.php";
$valor1 = $_GET['valor1'];
$lucasDb = new lucasDb();
$genero = $lucasDb->GeneroByID($valor1);
?>
<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <title>Gênero</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>

<body>

    <div class="conteudo font3">
        <div class="center font2">Editar Gênero</div>
        <form name="FromGenero" id="FromGenero" method="post" action="Controller/Editar_Genero.php" class="font1">

            Id_Generno:
            <input type="text" readonly="true" id="idgenero" class="key" name="idgenero" value="<?= $valor1 ?>"><br>

            Descrição:
            <input type="text" id="descricao" name="descricao" value="<?= $genero["descricao"] ?>"><br>
            
            <input type="submit" value="Alterar">
            <a href="genero.php"><input type="button" value="Voltar"></a>
        </form>
    </div>

</body>

</html>