<?php
require_once "Conexao/lucasDb.php";

$lucasDb = new lucasDb();
$filmes = $lucasDb->AllFilmes();
$filmes_view = $lucasDb-> FilmesView();
$AllGenero = $lucasDb-> AllGenero();
$AllSituacao = $lucasDb-> AllSituacao();
?>

<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <title>Filmes</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>

<body>
    <?php include "header.php"; ?>



    <div class="conteudo">

        <div class="center2 font2">Adicionar novo Filme</div>
        <form name="Fromfilmes" id="Fromfilmes" method="post" action="Controller/Adicionar_Filmes.php" class="font1">

            <br>
            Nome:
            <input type="text" id="nome" name="nome">
            <br><br>
            Classificação:
            <input type="text" id="classificacao" name="classificacao">
            <br><br>
            Duração:
            <input type="number" id="duracao" name="duracao">
            <br><br>
            Descrição:
            <input type="text" id="descricao" name="descricao">
            (sinopse)
            <br><br>
            Tipo:
            <input type="text" id="tipo" name="tipo">
            (dublado / legendado)
            <br><br>
            Gênero:
            <select id="idgenero" name="idgenero">
                <?php foreach ($AllGenero as $genero) { ?>
                    <option value="<?= $genero["idgenero"]; ?>"><?= $genero["descricao"]; ?></option>
                <?php   }  ?>
            </select><br><br>
            Situação:
            <select id="idsituacao" name="idsituacao">
                <?php foreach ($AllSituacao as $situacao) { ?>
                    <option value="<?= $situacao["idsituacao"]; ?>"><?= $situacao["descricao"]; ?></option>
                <?php   }  ?>
            </select><br><br>
        
            <input type="submit" value="cadastrar">
        </form>

        <br><br>

        <div class="center2 font2">Filmes</div>
        <div style="overflow: auto; width: 800px; height:300px; border:solid 1px">
            <table class="tabela" id="nj" style="width:1000px">
                <thead>
                    <tr>
                        <th>ID Filme</th>
                        <th>Nome</th>
                        <th>Classificação</th>
                        <th>Duração</th>
                        <th>Descrição</th>
                        <th>Tipo</th>
                        <th>Gênero</th>
                        <th>Situação</th>
                        <th>Ação</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($filmes as $c) { ?>
                        <tr>
                            <td><?= $c["idFilmes"] ?></td>
                            <td><?= $c["nome"] ?></td>
                            <td><?= $c["classificacao"] ?></td>
                            <td><?= $c["duracao"] ?></td>
                            <td><?= $c["descricao"] ?></td>
                            <td><?= $c["tipo"] ?></td>
                            <td><?= $c["genero_idgenero"] ?></td>
                            <td><?= $c["situacao_idsituacao"] ?></td>
                            <td>
                                <a href="EditarFilmes.php?valor1=<?=  $c["idFilmes"] ?>">Editar</a>
                                <a href="Controller/Excluir_Filmes.php?valor1=<?= $c["idFilmes"] ?>">Excluir</a>
                            </td>
                        </tr>
                    <?php   }  ?>
                </tbody>
            </table>
        </div>

        <div class="center2 font2">Filmes (VIEW com JOIN)</div>
        <div style="overflow: auto; width: 800px; height:300px; border:solid 1px">
            <table class="tabela" id="nj" style="width:1000px">
                <thead>
                    <tr>
                        <th>ID Filme</th>
                        <th>Nome</th>
                        <th>Classificação</th>
                        <th>Duração</th>
                        <th>Descrição</th>
                        <th>Tipo</th>
                        <th>Gênero</th>
                        <th>Situação</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($filmes_view as $a) { ?>
                        <tr>
                            <td><?= $a["idFilmes"] ?></td>
                            <td><?= $a["nome"] ?></td>
                            <td><?= $a["classificacao"] ?></td>
                            <td><?= $a["duracao"] ?></td>
                            <td><?= $a["descricao"] ?></td>
                            <td><?= $a["tipo"] ?></td>
                            <td><?= $a["genero"] ?></td>
                            <td><?= $a["situacao"] ?></td>
                        </tr>
                    <?php   }  ?>
                </tbody>
            </table>
        </div>
        <br><br>
</body>

</html>