<?php
require_once "Conexao/operadores.php";
$valor1 = $_GET['valor1'];
$valor2 = $_GET['valor2'];
$operadores = new operadores();
$cadeiras = $operadores->Mapa_SalaByID($valor1, $valor2);
$ListarIdSala = $operadores->ListarIdSala();
$ListarCadeiraFileira = $operadores->ListarCadeiraFileira();
$ListarCadeiraNumero = $operadores->ListarCadeiraNumero();
?>

<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <title>Mapa Sala</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>

<body>

    <div class="conteudo">
        <div class="center font1">Editar Cadeira</div>
        <form name="FromCadeira" id="FromCadeira" method="post" action="Controller/Editar_Mapa_sala.php" class="font1">

            ID Sala:
            <input readonly="true" type="text" id="sala_idSala" class="key" name="sala_idSala" value="<?= $cadeiras["sala_idSala"] ?>"><br> <!-- Chave Primaria não pode alterar -->
            Cadeira Fileira:
            <select id="cadeira_fileira" name="cadeira_fileira">
                <?php foreach ($ListarCadeiraFileira as $CadeiraFileira) { ?>
                    <option value="<?= $CadeiraFileira["fileira"]; ?>"> <?= $CadeiraFileira["fileira"]; ?></option>
                <?php   }  ?>
            </select><br>
            Cadeira Numero:
            <select id="cadeira_numero" name="cadeira_numero">
                <?php foreach ($ListarCadeiraNumero as $CadeiraNumero) { ?>
                    <option value="<?= $CadeiraNumero["numero"]; ?>"> <?= $CadeiraNumero["numero"]; ?></option>
                <?php   }  ?>
            </select><br><br>
            <input type="text" hidden id="cadeira_numero_old" name="cadeira_numero_old" value="<?= $cadeiras["cadeira_numero"] ?>">
            <input type="submit" value="Alterar">
            <a href="mapa_sala.php"><input type="button" value="Voltar"></a>
        </form>
    </div>

</body>

</html>