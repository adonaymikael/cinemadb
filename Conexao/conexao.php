<?php

abstract class Conexao {

    private static $instance;

    /**
     * 
     * @return PDO
     */
    
    public static function getInstance() {
        $server = getenv("DB_SERVER");
        $user = getenv("DB_USER");
        $password = getenv("DB_PASS");
        $dbname = getenv("DB_NAME");

        if (empty($server)) {
            $server = 'localhost';
        }
        if (empty($user)) {
            $user = 'root';
        }
        if (empty($password)) {
            $password = '';
        }
        if (empty($dbname)) {
            $dbname = 'CinemaDB';
        }        

        try {
            if (!isset(self::$instance)) {
                self::$instance = new PDO("mysql:host=".$server.";dbname=".$dbname, $user, $password);
                self::$instance->exec('SET CHARACTER SET utf8');
                self::$instance->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            }
            return self::$instance;
        } catch (PDOException $exc) {
            echo "Erro ao conectar o banco de dados :" . $exc->getMessage();
        }
    }

}
