<?php
//include("config_db.php");
//print_r($pdo);

require_once "conexao.php";

class lucasDb{  

    public $pdo = null;

    public function __construct() {
        $this->pdo = Conexao::getInstance();
    }

    //Métodos Gênero

    //GET ALL
    public static function AllGenero() {
        $sql = "SELECT * from genero";
        $pdo = Conexao::getInstance();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    } 

    //GET by ID
    public static function GeneroByID($id) {
        $sql = "SELECT * from genero WHERE idgenero =? ";
        $pdo = Conexao::getInstance();
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result  = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //INPUT
    public static function cadastroGenero(){
        $pdo  = Conexao::getInstance();
        $descricao = filter_input(INPUT_POST, 'descricao');

        $sql  = 'INSERT INTO genero (descricao)VALUES (?)';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $descricao);
        $stmt->execute();
    }

    //DELETE
    public static function excluirGenero($id){
        $pdo  = Conexao::getInstance();
        $sql  = 'DELETE FROM genero WHERE idgenero= ? ;';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
    }

    //UPDATE
    public static function alterarGenero(){
        $pdo  = Conexao::getInstance();
        $descricao = filter_input(INPUT_POST, 'descricao');
        $idgenero = filter_input(INPUT_POST, 'idgenero');

        $sql  = 'UPDATE genero SET descricao=? WHERE idgenero = ?;';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $descricao);
        $stmt->bindValue(2, $idgenero);
        
        $stmt->execute();
    }

    //Métodos Filmes

    //GET ALL
    public static function AllFilmes() {
        $sql = "SELECT * from filmes";
        $pdo = Conexao::getInstance();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    } 

    //GET by ID
    public static function FilmesByID($id) {
        $sql = "SELECT * from filmes WHERE idfilmes =? ";
        $pdo = Conexao::getInstance();
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result  = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    //GET view
    public static function FilmesView(){
        $sql = "SELECT * from filmes_sit_genero";
        $pdo = Conexao::getInstance();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    //INPUT 
    public static function cadastroFilmes(){
        $pdo  = Conexao::getInstance();
        $nome = filter_input(INPUT_POST, 'nome');
        $classificacao = filter_input(INPUT_POST, 'classificacao');
        $duracao = filter_input(INPUT_POST, 'duracao');
        $descricao = filter_input(INPUT_POST, 'descricao');
        $tipo = filter_input(INPUT_POST, 'tipo');
        $idgenero = filter_input(INPUT_POST, 'idgenero');
        $idsituacao = filter_input(INPUT_POST, 'idsituacao');

        $sql  = 'INSERT INTO filmes (nome, classificacao, duracao, descricao, tipo, genero_idgenero, situacao_idsituacao) VALUES (?, ?, ?, ?, ?, ?, ?);';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $nome);
        $stmt->bindValue(2, $classificacao);
        $stmt->bindValue(3, $duracao);
        $stmt->bindValue(4, $descricao);
        $stmt->bindValue(5, $tipo);
        $stmt->bindValue(6, $idgenero);
        $stmt->bindValue(7, $idsituacao);
        $stmt->execute();
    }

    //DELETE
    public static function excluirFilmes($id){
        $pdo  = Conexao::getInstance();
        $sql  = 'DELETE FROM filmes WHERE idFilmes= ? ;';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
    }

    //UPDATE
    public static function alterarFilmes(){
        $pdo  = Conexao::getInstance();
        $idfilmes = filter_input(INPUT_POST, 'idfilmes');
        $nome = filter_input(INPUT_POST, 'nome');
        $classificacao = filter_input(INPUT_POST, 'classificacao');
        $duracao = filter_input(INPUT_POST, 'duracao');
        $descricao = filter_input(INPUT_POST, 'descricao');
        $tipo = filter_input(INPUT_POST, 'tipo');
        $idgenero = filter_input(INPUT_POST, 'idgenero');
        $idsituacao = filter_input(INPUT_POST, 'idsituacao');

        $sql  = 'UPDATE filmes SET nome=?, classificacao=?, duracao=?, descricao=?, tipo=?, genero_idgenero=?, situacao_idsituacao=? WHERE idfilmes=?;';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $nome);
        $stmt->bindValue(2, $classificacao);
        $stmt->bindValue(3, $duracao);
        $stmt->bindValue(4, $descricao);
        $stmt->bindValue(5, $tipo);
        $stmt->bindValue(6, $idgenero);
        $stmt->bindValue(7, $idsituacao);
        $stmt->bindValue(8, $idfilmes);
        $stmt->execute();
    }

    //GET ALL SITUACAO
    public static function AllSituacao() {
        $sql = "SELECT * from situacao";
        $pdo = Conexao::getInstance();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    } 
}
