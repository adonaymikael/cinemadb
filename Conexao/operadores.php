<?php
//include("config_db.php");
//print_r($pdo);

require_once "conexao.php";

class operadores{  

    public $pdo = null;

    public function __construct() {
        $this->pdo = Conexao::getInstance();
    }

         //Metodos Cadeira
    public static function AllCadeira() {
        $sql = "SELECT * from cadeira";
        $pdo = Conexao::getInstance();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function AllCadeira_cadeiraReserva() {
        $sql = "SELECT * FROM Cadeira_E_Reserva;";
        $pdo = Conexao::getInstance();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function getReservasAndTipoPagamento(){
        $sql = "SELECT * FROM Reserva_E_Tipo_Pagamento;";
        $pdo = Conexao::getInstance();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function CadeiraByID($numero,$fileira) {
        $sql = "SELECT * from cadeira WHERE numero =? and fileira=?";
        $pdo = Conexao::getInstance();
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $numero);
        $stmt->bindValue(2, $fileira);
        $stmt->execute();
        $result  = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function cadastroCadeira(){
        $pdo  = Conexao::getInstance();
        $numero = filter_input(INPUT_POST, 'numero');
        $status = filter_input(INPUT_POST, 'status');
        $tipo = filter_input(INPUT_POST, 'tipo');
        $fileira = filter_input(INPUT_POST, 'fileira');

        $sql  = 'INSERT INTO cadeira(fileira, numero, status, tipo) VALUES (?, ?, ?, ?)';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $fileira);
        $stmt->bindValue(2, $numero);
        $stmt->bindValue(3, $status);
        $stmt->bindValue(4, $tipo);
        $stmt->execute();
    }

    public static function cadastroTipoPagamento(){
        $pdo  = Conexao::getInstance();
        $descricao = filter_input(INPUT_POST, 'descricao');

        $sql  = 'INSERT INTO tipo_pagamento(descricao) VALUES (?)';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $descricao);
        $stmt->execute();
    }

    public static function excluirCadeira($numero,$fileira){
        $pdo  = Conexao::getInstance();
        $sql  = 'DELETE FROM cadeira WHERE numero= ? and fileira= ?;';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $numero);
        $stmt->bindValue(2, $fileira);
        $stmt->execute();
    }

    public static function alterarCadeira(){
        $pdo  = Conexao::getInstance();
        $numero = filter_input(INPUT_POST, 'numero');
        $status = filter_input(INPUT_POST, 'status');
        $tipo = filter_input(INPUT_POST, 'tipo');
        $fileira = filter_input(INPUT_POST, 'fileira');

        $sql  = 'UPDATE cadeira SET status=?, tipo=? WHERE numero=? and fileira=?;';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $status);
        $stmt->bindValue(2, $tipo);
        $stmt->bindValue(3, $numero);
        $stmt->bindValue(4, $fileira);
        $stmt->execute();
    }

         //Metodos Mapa-Sala
    public static function AllMapa_Salas() {
        $sql = "SELECT * from mapa_sala";
        $pdo = Conexao::getInstance();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function ListarIdSala() {
        $sql = "SELECT idSala FROM sala";
        $pdo = Conexao::getInstance();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function ListarCadeiraFileira() {
        $sql = "SELECT DISTINCT fileira FROM cadeira;";
        $pdo = Conexao::getInstance();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function ListarCadeiraNumero() {
        $sql = "SELECT DISTINCT numero FROM cadeira;";
        $pdo = Conexao::getInstance();
        $stmt = $pdo->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function cadastroMapa_Salas(){
        $pdo  = Conexao::getInstance();
        $idSala = filter_input(INPUT_POST, 'sala_idSala');
        $cadeiraFileira = filter_input(INPUT_POST, 'cadeira_fileira');
        $cadeiraNumero = filter_input(INPUT_POST, 'cadeira_numero');
        echo $idSala;

        $sql  = 'INSERT INTO mapa_sala (sala_idSala, cadeira_fileira, cadeira_numero) VALUES (?, ?, ?)';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $idSala);
        $stmt->bindValue(2, $cadeiraFileira);
        $stmt->bindValue(3, $cadeiraNumero);
        $stmt->execute();
    }

    public static function Mapa_SalaByID($numero,$fileira) {
        $sql = "SELECT * from mapa_sala WHERE sala_idSala =? and cadeira_numero=?";
        $pdo = Conexao::getInstance();
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $numero);
        $stmt->bindValue(2, $fileira);
        $stmt->execute();
        $result  = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function alterarMapa_sala(){
        $pdo  = Conexao::getInstance();
        $idSala = filter_input(INPUT_POST, 'sala_idSala');
        $cadeiraFileira = filter_input(INPUT_POST, 'cadeira_fileira');
        $cadeiraNumero = filter_input(INPUT_POST, 'cadeira_numero');
        $cadeiraNumeroOld = filter_input(INPUT_POST, 'cadeira_numero_old');

        $sql  = 'UPDATE mapa_sala SET cadeira_numero=?, cadeira_fileira=? WHERE cadeira_numero=? and sala_idSala=?;';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $cadeiraNumero);
        $stmt->bindValue(2, $cadeiraFileira);
        $stmt->bindValue(3, $cadeiraNumeroOld);
        $stmt->bindValue(4, $idSala);
        $stmt->execute();
    }

    public static function excluirMapa_sala($valor1,$valor2){
        $pdo  = Conexao::getInstance();
        $sql  = 'DELETE FROM mapa_sala WHERE sala_idSala= ? and cadeira_numero= ?;';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $valor1);
        $stmt->bindValue(2, $valor2);
        $stmt->execute();
    }

    public static function AllEndereco(){
        $sql     = "SELECT * FROM endereco;";
        $pdo     = Conexao::getInstance();
        $stmt    = $pdo->prepare($sql);
        $stmt->execute();
        $result  = $stmt->fetchAll();
        return $result;
    }

    public static function cadastroCinema(){
        $pdo      = Conexao::getInstance();
        $nome     = filter_input(INPUT_POST, 'nome');
        $cnpj     = filter_input(INPUT_POST, 'cnpj');
        $endereco = filter_input(INPUT_POST, 'end_cinema');

        $sql  = 'INSERT INTO cinema (nome, cnpj, endereco_idEndereco) VALUES (?, ?, ?);';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $nome);
        $stmt->bindValue(2, $cnpj);
        $stmt->bindValue(3, $endereco);
        $stmt->execute();
    }

    public static function cinemaByID($id){
        $sql     = "SELECT * FROM cinema C INNER JOIN endereco E ON(E.idendereco = C.endereco_idendereco) WHERE idcinema=?;";
        $pdo     = Conexao::getInstance();
        $stmt    = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result  = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function getAllUsers(){
        $sql     = "SELECT * FROM usuario;";
        $pdo     = Conexao::getInstance();
        $stmt    = $pdo->prepare($sql);
        $stmt->execute();
        $result  = $stmt->fetchAll();
        
        return $result;
    }

    public static function alterarCinema(){
        $pdo      = Conexao::getInstance();
        $idCinema = filter_input(INPUT_POST, 'id');
        $nome     = filter_input(INPUT_POST, 'nome');
        $cnpj     = filter_input(INPUT_POST, 'cnpj');
        $endereco = filter_input(INPUT_POST, 'end_cinema');

        $sql  = 'UPDATE cinema SET nome=?, cnpj=?, endereco_idEndereco=?  WHERE idCinema=?;';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $nome);
        $stmt->bindValue(2, $cnpj);
        $stmt->bindValue(3, $endereco);
        $stmt->bindValue(4, $idCinema);
        $stmt->execute();
    }

    public static function excluirCinema($id){
        $pdo  = Conexao::getInstance();
        $sql  = 'DELETE FROM cinema WHERE idCinema=?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
    }

    public static function cadastroSessao(){
        $pdo       = Conexao::getInstance();
        $id_cinema = filter_input(INPUT_POST, 'cinema');
        $id_filme  = filter_input(INPUT_POST, 'filme');
        $id_sala   = filter_input(INPUT_POST, 'sala');
        $data      = filter_input(INPUT_POST, 'data');
        $duracao   = filter_input(INPUT_POST, 'duracao');

        $sql  = 'INSERT INTO sessao (cinema_idCinema, filmes_idFilmes, sala_idSala, data, duracao) VALUES (?, ?, ?, ?, ?);';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id_cinema);
        $stmt->bindValue(2, $id_filme);
        $stmt->bindValue(3, $id_sala);
        $stmt->bindValue(4, $data);
        $stmt->bindValue(5, $duracao);
        $stmt->execute();
    }

    public static function cadastroUsuario(){
        $pdo       = Conexao::getInstance();
        $nome = filter_input(INPUT_POST, 'nome');
        $cpf  = filter_input(INPUT_POST, 'cpf');
        $senha   = filter_input(INPUT_POST, 'pass');
        $sexo      = filter_input(INPUT_POST, 'genero');
        $idade   = filter_input(INPUT_POST, 'idade');
        $email   = filter_input(INPUT_POST, 'email');

        $sql  = 'INSERT INTO usuario (nome, cpf, senha, sexo, idade, email) VALUES (?, ?, ?, ?, ?, ?);';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $nome);
        $stmt->bindValue(2, $cpf);
        $stmt->bindValue(3, $senha);
        $stmt->bindValue(4, $sexo);
        $stmt->bindValue(5, $idade);
        $stmt->bindValue(6, $email);
        $stmt->execute();
    }

    public static function alterarTipoPagamento(){
        $pdo      = Conexao::getInstance();
        $id       = filter_input(INPUT_POST, 'idtipo_pagamento');
        $descricao   = filter_input(INPUT_POST, 'descricao');

        $sql  = 'UPDATE tipo_pagamento SET descricao=? WHERE idtipo_pagamento=?;';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $descricao);
        $stmt->bindValue(2, $id);
        $stmt->execute();
    }

    public static function paymentTypeById($id){
        $sql = "SELECT * from tipo_pagamento WHERE idtipo_pagamento = ?";
        $pdo = Conexao::getInstance();
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result  = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public static function excluirTipoPagamento($id){
        $pdo  = Conexao::getInstance();
        $sql  = 'DELETE FROM tipo_pagamento WHERE idtipo_pagamento=?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
    }


    public static function getAllPaymentType(){
        $sql     = "SELECT * FROM tipo_pagamento;";
        $pdo     = Conexao::getInstance();
        $stmt    = $pdo->prepare($sql);
        $stmt->execute();
        $result  = $stmt->fetchAll();
        
        return $result;
    }

    public static function alterarUsuario(){
        $pdo      = Conexao::getInstance();
        $id       = filter_input(INPUT_POST, 'idusuario');
        $nome   = filter_input(INPUT_POST, 'nome');
        $cpf    = filter_input(INPUT_POST, 'cpf');
        $senha    = filter_input(INPUT_POST, 'pass');
        $sexo     = filter_input(INPUT_POST, 'genero');
        $idade  = filter_input(INPUT_POST, 'idade');
        $email  = filter_input(INPUT_POST, 'email');

        $sql  = 'UPDATE usuario SET nome=?, cpf=?, senha=?, sexo=?, idade=?, email=? WHERE idusuario=?;';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $nome);
        $stmt->bindValue(2, $cpf);
        $stmt->bindValue(3, $senha);
        $stmt->bindValue(4, $sexo);
        $stmt->bindValue(5, $idade);
        $stmt->bindValue(6, $email);
        $stmt->bindValue(7, $id);
        $stmt->execute();

    }

    public static function userById($id){
        $sql = "SELECT * from usuario WHERE idusuario = ?";
        $pdo = Conexao::getInstance();
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result  = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;

    }

    public static function excluirUsuario($id){
        $pdo  = Conexao::getInstance();
        $sql  = 'DELETE FROM usuario WHERE idusuario=?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
    }


    public static function alterarSessao(){
        $pdo      = Conexao::getInstance();
        $id       = filter_input(INPUT_POST, 'id');
        $cinema   = filter_input(INPUT_POST, 'cinema');
        $filme    = filter_input(INPUT_POST, 'filme');
        $sala     = filter_input(INPUT_POST, 'sala');
        $data     = filter_input(INPUT_POST, 'data');
        $duracao  = filter_input(INPUT_POST, 'duracao');

        $sql  = 'UPDATE sessao SET cinema_idCinema=?, filmes_idFilmes=?, sala_idSala=?, data=?, duracao=? WHERE idSessao=?;';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $cinema);
        $stmt->bindValue(2, $filme);
        $stmt->bindValue(3, $sala);
        $stmt->bindValue(4, $data);
        $stmt->bindValue(5, $duracao);
        $stmt->bindValue(6, $id);
        $stmt->execute();
    }

    public static function excluirSessao($id){
        $pdo  = Conexao::getInstance();
        $sql  = 'DELETE FROM sessao WHERE idSessao=?';
        $stmt = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
    }

    public static function listarFilmes(){
        $sql     = "SELECT * FROM filmes;";
        $pdo     = Conexao::getInstance();
        $stmt    = $pdo->prepare($sql);
        $stmt->execute();
        $result  = $stmt->fetchAll();
        return $result;
    }

    public static function listarSala(){
        $sql     = "SELECT * FROM sala;";
        $pdo     = Conexao::getInstance();
        $stmt    = $pdo->prepare($sql);
        $stmt->execute();
        $result  = $stmt->fetchAll();
        return $result;
    }

    public static function listarSessao(){
        $sql     = "SELECT * FROM Dados_Sessao;";
        $pdo     = Conexao::getInstance();
        $stmt    = $pdo->prepare($sql);
        $stmt->execute();
        $result  = $stmt->fetchAll();
        return $result;
    }

    public static function sessaoByID($id){
        $sql     = "SELECT SE.idSessao, C.nome cinema, F.nome filme, SA.numero, SE.data, SE.duracao FROM sessao SE 
                        INNER JOIN cinema C  ON(C.idCinema = SE.cinema_idCinema) 
                        INNER JOIN sala   SA ON(SA.idSala  = SE.sala_idSala) 
                        INNER JOIN filmes F  ON(F.idFilmes = SE.filmes_idFilmes) 
                        WHERE SE.idSessao=?";
        $pdo     = Conexao::getInstance();
        $stmt    = $pdo->prepare($sql);
        $stmt->bindValue(1, $id);
        $stmt->execute();
        $result  = $stmt->fetch(PDO::FETCH_ASSOC);
        return $result;
    }
    public static function AllCinemas(){
        $sql     = "SELECT * FROM cinema;";
        $pdo     = Conexao::getInstance();
        $stmt    = $pdo->prepare($sql);
        $stmt->execute();
        $result  = $stmt->fetchAll();
        return $result;
    }
    public static function AllFilmes(){
        $sql     = "SELECT * FROM filmes;";
        $pdo     = Conexao::getInstance();
        $stmt    = $pdo->prepare($sql);
        $stmt->execute();
        $result  = $stmt->fetchAll();
        return $result;
    }
    public static function AllSalas(){
        $sql     = "SELECT * FROM sala;";
        $pdo     = Conexao::getInstance();
        $stmt    = $pdo->prepare($sql);
        $stmt->execute();
        $result  = $stmt->fetchAll();
        return $result;
    }
}
