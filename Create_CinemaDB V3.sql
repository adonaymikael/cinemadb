﻿CREATE DATABASE  IF NOT EXISTS `CinemaDB`;
USE `CinemaDB`;

CREATE TABLE `cadeira` (
  `fileira` varchar(255) NOT NULL,
  `numero` int(11) NOT NULL,
  `status` varchar(255) NOT NULL,
  `tipo` varchar(255) NOT NULL,
  PRIMARY KEY (`fileira`,`numero`)
)ENGINE=InnoDB CHARACTER SET=utf8 COLLATE utf8_unicode_ci;

INSERT INTO `cadeira` VALUES ('A',1,'Disponivel','Acompanhante'),('A',2,'Disponivel','Cadeirante'),('A',3,'Disponivel','Cadeirante'),('A',4,'Disponivel','Acompanhante'),('A',5,'Disponivel','Simples'),('A',6,'Disponivel','Simples'),('A',7,'Disponivel','Simples'),('A',8,'Disponivel','Simples'),('A',9,'Disponivel','Simples'),('A',10,'Disponivel','Simples'),('A',11,'Disponivel','Acompanhante'),('A',12,'Disponivel','Cadeirante'),('A',13,'Disponivel','Cadeirante'),('A',14,'Disponivel','Acompanhante');

CREATE TABLE `endereco` (
  `idEndereco` int(11) NOT NULL AUTO_INCREMENT,
  `logradouro` varchar(255) NOT NULL,
  `numero` int(11) NOT NULL,
  `cidade` varchar(255) NOT NULL,
  `estado` varchar(255) NOT NULL,
  `complemento` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idEndereco`)
)ENGINE=InnoDB CHARACTER SET=utf8 COLLATE utf8_unicode_ci;

INSERT INTO `endereco` VALUES (1,'Alameda Shopping',71985,'Taguatinga Sul',' Distrito Federal','CSB 2 Lotes 01 a 04 , No inicio da comercial Sul '),(2,'Taguatinga Shopping',71046,'Taguatinga Sul',' Distrito Federal',' QS 1 Lote 40, Pistão Sul '),(3,'ParkShopping',70319,'Guará',' Distrito Federal','SAI/SO Área 6580, Ao lado do Metrô, via EPIA ');

CREATE TABLE `genero` (
  `idgenero` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL,
  PRIMARY KEY (`idgenero`)
)ENGINE=InnoDB CHARACTER SET=utf8 COLLATE utf8_unicode_ci;

INSERT INTO `genero` VALUES (1,'ação'),(2,'comédia'),(3,'drama'),(4,'ficção científica'),(5,'animação'),(6,'aventura'),(7,'documentário'),(8,'guerra'),(9,'musical'),(10,'suspense'),(11,'terror');


CREATE TABLE `situacao` (
  `idsituacao` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL,
  PRIMARY KEY (`idsituacao`)
)ENGINE=InnoDB CHARACTER SET=utf8 COLLATE utf8_unicode_ci;

INSERT INTO `situacao` VALUES (1,'em cartaz'),(2,'em breve'),(3,'fora de cartaz');

CREATE TABLE `tipo_pagamento` (
  `idtipo_pagamento` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL,
  PRIMARY KEY (`idtipo_pagamento`)
)ENGINE=InnoDB CHARACTER SET=utf8 COLLATE utf8_unicode_ci;

INSERT INTO `tipo_pagamento` VALUES (1,'Cartão de crédito'),(2,'Milhas'),(3,'Boleto');

CREATE TABLE `usuario` (
  `idusuario` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `cpf` varchar(11) NOT NULL,
  `senha` varchar(255) NOT NULL,
  `sexo` varchar(255) DEFAULT NULL,
  `idade` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idusuario`)
)ENGINE=InnoDB CHARACTER SET=utf8 COLLATE utf8_unicode_ci;

INSERT INTO `usuario` VALUES (1,'Roberto Carlos','01218391242','123','Masculino','23','robertinho.caca@sempreceub.com'),(2,'Ana Clara','51444276542','785','Feminino','19','annna.clara@gmail.com'),(3,'Cedric Souza','11237675611','896','Masculino','30','ccc.za@outlook.com');


CREATE TABLE `cinema` (
  `idCinema` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `cnpj` varchar(255) NOT NULL,
  `endereco_idEndereco` int(11) NOT NULL,
  PRIMARY KEY (`idCinema`),
  KEY `fk_cinema_endereco_idx` (`endereco_idEndereco`),
  CONSTRAINT `fk_cinema_endereco` FOREIGN KEY (`endereco_idEndereco`) REFERENCES `endereco` (`idEndereco`) ON DELETE NO ACTION ON UPDATE NO ACTION
)ENGINE=InnoDB CHARACTER SET=utf8 COLLATE utf8_unicode_ci;

INSERT INTO `cinema` VALUES (1,'Cinema da Cidade','63.957.590/0001-65',1),(2,'Cinema do Bairro','33.114.188/0001-58',2),(3,'Cineminha','64.328.976/0001-70',3);


CREATE TABLE `filmes` (
  `idFilmes` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL,
  `classificacao` varchar(255) NOT NULL,
  `duracao` int(11) NOT NULL,
  `descricao` varchar(1000) NOT NULL,
  `tipo` varchar(255) NOT NULL,
  `genero_idgenero` int(11) NOT NULL,
  `situacao_idsituacao` int(11) NOT NULL,
  PRIMARY KEY (`idFilmes`),
  KEY `fk_filmes_genero1_idx` (`genero_idgenero`),
  KEY `fk_filmes_situacao1_idx` (`situacao_idsituacao`),
  CONSTRAINT `fk_filmes_genero1` FOREIGN KEY (`genero_idgenero`) REFERENCES `genero` (`idgenero`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_filmes_situacao1` FOREIGN KEY (`situacao_idsituacao`) REFERENCES `situacao` (`idsituacao`) ON DELETE NO ACTION ON UPDATE NO ACTION
)ENGINE=InnoDB CHARACTER SET=utf8 COLLATE utf8_unicode_ci;

INSERT INTO `filmes` VALUES (1,'IT - capítulo 2','16',169,'27 anos depois dos eventos de \"It - Parte 1\", o grupo de adolescentes que fazia parte do \"Losers Club\" - o clube dos perdedores - realiza uma reunião. No entanto, o que parece ser apenas um reencontro de velhos amigos acaba se tornando em uma verdadeira e sangrenta batalha quando Pennywise (Bill Skarsgard), o palhaço retorna. Classificação indicativa 16 anos, contém drogas, violência extrema e linguagem imprópria.','dublado',11,1),(2,'Gato de Botas','livre',90,'Muito antes de conhecer o ogro Shrek e sua turma, Gato de Botas (Antonio Banderas) vai viver uma grande aventura ao lado de Humpty Dumpty (Zach Galifianakis) e Kitty Pata Mansa (Salma Hayek). Dipostos a roubar os feijões mágicos do casal fora da lei Jack (Billy Bob Thornton) e Jill (Amy Sedaris), o trio quer mesmo é botar as mãos na famosa gansa que bota ovos de ouro. Mas algumas coisas não estavam nos planos e Gato vai descobrir, meio atrasado, que tem um grande problema pela frente para conseguir limpar o que ficou para trás: a sua honra.','dublado',5,2),(3,'Ad Astra: Rumo às Estrelas','12',150,'Roy McBride (Brad Pitt) é um engenheiro espacial, portador de um leve grau de autismo, que decide empreender a maior jornada de sua vida: viajar para o espaço, cruzar a galáxia e tentar descobrir o que aconteceu com seu pai, um astronauta que se perdeu há vinte anos atrás no caminho para Netuno.','legendado',4,3);


CREATE TABLE `sala` (
  `idSala` int(11) NOT NULL AUTO_INCREMENT,
  `numero` int(11) NOT NULL,
  `tipo` varchar(255) NOT NULL,
  `capacidade` varchar(255) NOT NULL,
  `cinema_idCinema` int(11) NOT NULL,
  PRIMARY KEY (`idSala`),
  KEY `fk_sala_cinema1_idx` (`cinema_idCinema`),
  CONSTRAINT `fk_sala_cinema1` FOREIGN KEY (`cinema_idCinema`) REFERENCES `cinema` (`idCinema`) ON DELETE NO ACTION ON UPDATE NO ACTION
)ENGINE=InnoDB CHARACTER SET=utf8 COLLATE utf8_unicode_ci;

INSERT INTO `sala` VALUES (1,1,'2D','14',1),(2,2,'3D','11',1),(3,3,'3D','14',2),(4,4,'2D','11',2),(5,5,'2D','14',3),(6,6,'3D','11',3);


CREATE TABLE `sessao` (
  `idsessao` int(11) NOT NULL AUTO_INCREMENT,
  `cinema_idCinema` int(11) NOT NULL,
  `filmes_idFilmes` int(11) NOT NULL,
  `sala_idSala` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `duracao` int(11) NOT NULL,
  PRIMARY KEY (`idsessao`),
  KEY `fk_sessao_cinema1_idx` (`cinema_idCinema`),
  KEY `fk_sessao_filmes1_idx` (`filmes_idFilmes`),
  KEY `fk_sessao_sala1_idx` (`sala_idSala`),
  CONSTRAINT `fk_sessao_cinema1` FOREIGN KEY (`cinema_idCinema`) REFERENCES `cinema` (`idCinema`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sessao_filmes1` FOREIGN KEY (`filmes_idFilmes`) REFERENCES `filmes` (`idFilmes`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sessao_sala1` FOREIGN KEY (`sala_idSala`) REFERENCES `sala` (`idSala`) ON DELETE NO ACTION ON UPDATE NO ACTION
)ENGINE=InnoDB CHARACTER SET=utf8 COLLATE utf8_unicode_ci;

INSERT INTO `sessao` VALUES (1,2,3,3,'2019-09-22 00:00:00',126),(2,1,1,3,'2019-10-01 00:00:00',200),(3,2,1,1,'2019-09-24 00:00:00',164);


CREATE TABLE `mapa_sala` (
  `sala_idSala` int(11) NOT NULL,
  `cadeira_fileira` varchar(255) NOT NULL,
  `cadeira_numero` int(11) NOT NULL,
  PRIMARY KEY (`sala_idSala`,`cadeira_fileira`,`cadeira_numero`),
  KEY `fk_sala_has_cadeira_cadeira1_idx` (`cadeira_fileira`,`cadeira_numero`),
  KEY `fk_sala_has_cadeira_sala1_idx` (`sala_idSala`),
  CONSTRAINT `fk_sala_has_cadeira_cadeira1` FOREIGN KEY (`cadeira_fileira`, `cadeira_numero`) REFERENCES `cadeira` (`fileira`, `numero`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_sala_has_cadeira_sala1` FOREIGN KEY (`sala_idSala`) REFERENCES `sala` (`idSala`) ON DELETE NO ACTION ON UPDATE NO ACTION
)ENGINE=InnoDB CHARACTER SET=utf8 COLLATE utf8_unicode_ci;

INSERT INTO `mapa_sala` VALUES (1,'A',1),(1,'A',2),(1,'A',3),(1,'A',4),(1,'A',5),(1,'A',6),(1,'A',7),(1,'A',8),(1,'A',9),(1,'A',10),(1,'A',11),(1,'A',12),(1,'A',13),(1,'A',14),(2,'A',1),(2,'A',2),(2,'A',3),(2,'A',4),(2,'A',5),(2,'A',6),(2,'A',7),(2,'A',8),(2,'A',9),(2,'A',10),(2,'A',11),(2,'A',12),(2,'A',13),(2,'A',14),(3,'A',1),(3,'A',2),(3,'A',3),(3,'A',4),(3,'A',5),(3,'A',6),(3,'A',7),(3,'A',8),(3,'A',9),(3,'A',10),(3,'A',11);


CREATE TABLE `reserva` (
  `idReserva` int(11) NOT NULL AUTO_INCREMENT,
  `cadeira_idCadeira` int(11) NOT NULL,
  `pagamento_idPagamento` int(11) NOT NULL,
  `usuario_idusuario` int(11) NOT NULL,
  `sessao_idsessao` int(11) NOT NULL,
  `valor` decimal(6,2) NOT NULL,
  `codigo` int(11) NOT NULL,
  `data_venda` datetime DEFAULT NULL,
  `tipo_pagamento_idtipo_pagamento` int(11) NOT NULL,
  PRIMARY KEY (`idReserva`),
  KEY `fk_reserva_usuario1_idx` (`usuario_idusuario`),
  KEY `fk_reserva_sessao1_idx` (`sessao_idsessao`),
  KEY `fk_reserva_tipo_pagamento1_idx` (`tipo_pagamento_idtipo_pagamento`),
  CONSTRAINT `fk_reserva_sessao1` FOREIGN KEY (`sessao_idsessao`) REFERENCES `sessao` (`idsessao`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reserva_tipo_pagamento1` FOREIGN KEY (`tipo_pagamento_idtipo_pagamento`) REFERENCES `tipo_pagamento` (`idtipo_pagamento`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reserva_usuario1` FOREIGN KEY (`usuario_idusuario`) REFERENCES `usuario` (`idusuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
)ENGINE=InnoDB CHARACTER SET=utf8 COLLATE utf8_unicode_ci;

INSERT INTO `reserva` VALUES (2,1,1,1,1,10.59,67651,'2022-01-22 00:00:00',1),(3,2,2,2,2,34.30,74445,'2005-09-14 00:00:00',2),(4,3,3,3,3,300.50,5234,'2002-04-02 00:00:00',3);


CREATE TABLE `reserva_cadeira` (
  `reserva_idReserva` int(11) NOT NULL,
  `cadeira_fileira` varchar(255) NOT NULL,
  `cadeira_numero` int(11) NOT NULL,
  PRIMARY KEY (`reserva_idReserva`,`cadeira_fileira`,`cadeira_numero`),
  KEY `fk_reserva_has_cadeira_cadeira1_idx` (`cadeira_fileira`,`cadeira_numero`),
  KEY `fk_reserva_has_cadeira_reserva1_idx` (`reserva_idReserva`),
  CONSTRAINT `fk_reserva_has_cadeira_cadeira1` FOREIGN KEY (`cadeira_fileira`, `cadeira_numero`) REFERENCES `cadeira` (`fileira`, `numero`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_reserva_has_cadeira_reserva1` FOREIGN KEY (`reserva_idReserva`) REFERENCES `reserva` (`idReserva`) ON DELETE NO ACTION ON UPDATE NO ACTION
)ENGINE=InnoDB CHARACTER SET=utf8 COLLATE utf8_unicode_ci;

INSERT INTO `reserva_cadeira` VALUES (2,'A',1),(2,'A',2),(3,'A',5),(4,'A',8),(4,'A',9),(4,'A',10);



/* Criacao de Views */

CREATE VIEW `filmes_sit_genero` AS
select a.idFilmes, a.nome, a.classificacao, a.duracao, a.descricao, a.tipo, b.descricao genero, c.descricao situacao 
from filmes a
inner join genero b on a.genero_idgenero = b.idgenero
inner join situacao c on a.situacao_idsituacao = c.idsituacao
order by a.nome asc, a.classificacao asc;


CREATE VIEW Cadeira_E_Reserva AS SELECT * FROM cadeira A 
INNER JOIN reserva_cadeira B ON A.numero = B.cadeira_numero AND A.fileira = B.cadeira_fileira;


CREATE VIEW Reserva_E_Tipo_Pagamento AS SELECT R.codigo, R.valor, R.data_venda, T.descricao FROM reserva R
INNER JOIN tipo_pagamento T ON R.pagamento_idPagamento = T.idtipo_pagamento;


CREATE VIEW Dados_Sessao AS SELECT SE.idSessao, C.nome cinema, F.nome filme, SA.numero, SE.data, SE.duracao FROM sessao SE 
INNER JOIN cinema C  ON(C.idCinema = SE.cinema_idCinema) 
INNER JOIN sala   SA ON(SA.idSala  = SE.sala_idSala) 
INNER JOIN filmes F  ON(F.idFilmes = SE.filmes_idFilmes);