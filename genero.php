<?php
require_once "Conexao/lucasDb.php";

$lucasDb = new lucasDb();
$generos = $lucasDb->AllGenero();
?>

<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="UTF-8">
    <title>Gêneros</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>

<body>
    <?php include "header.php"; ?>



    <div class="conteudo">

        <div class="center2 font2">Adicionar novo Gênero</div>
        <form name="Fromgenero" id="Fromgenero" method="post" action="Controller/Adicionar_Genero.php" class="font1">
           <br>
            Descricao:
            <input type="text" id="descricao" name="descricao">
            <br><br>

            <input type="submit" value="cadastrar">
        </form>

        <br><br>

        <div class="center2 font2">Lista dos Gêneros</div>
        <div style="overflow: auto; width: 640px; height: 200px; border:solid 1px">
            <table class="tabela" id="sql" style="width:800px">
                <thead>
                    <tr>
                        <th>ID Gênero</th>
                        <th>Descrição</th>
                        <th>Ação</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($generos as $c) { ?>
                        <tr>
                            <td><?= $c["idgenero"] ?></td>
                            <td><?= $c["descricao"] ?></td>
                            <td>
                                <a href="EditarGenero.php?valor1=<?= $c["idgenero"] ?>">Editar</a>
                                <a href="Controller/Excluir_Genero.php?valor1=<?= $c["idgenero"] ?>">Excluir</a>
                            </td>
                        </tr>
                    <?php   }  ?>
                </tbody>
            </table>
        </div>

        <br><br>

</body>

</html>