<?php
require_once "Conexao/operadores.php";

$operadores     = new operadores();
$paymentTypes      = $operadores->getAllPaymentType();
$reservas_e_tipoPagamento = $operadores->getReservasAndTipoPagamento();
?>

<head>
    <meta charset="UTF-8">
    <title>Cadeira</title>
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>

<?php include "header.php"; ?>

<div class="conteudo">
    <h3>Adicionar Tipo de Pagamento</h3>
    <form method="post" action="Controller/Adicionar_Tipo_Pagamento.php">
        Descrição:
        <input type="text" name="descricao" id="descricao"><br><br>

        <input type="submit" value="Cadastrar">
    </form>

<div class="center2 font2">Lista de Tipos de Pagamento</div>
    <div style="overflow: auto; width: 640px; height: 200px; border:solid 1px">
        <table class="tabela" id="sql" style="width:800px; text-align:center;">
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Descrição</th>
                    <th>Ação</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($paymentTypes as $paymentType) { ?>
                    <tr>
                        <td style="text-align: center;"><?= $paymentType["idtipo_pagamento"] ?></td>
                        <td style="text-align: center;"><?= $paymentType["descricao"] ?></td>
                        <td>
                            <a href="EditarTipoPagamento.php?idtipo_pagamento=<?= $paymentType["idtipo_pagamento"] ?>">Editar</a>
                            <a href="Controller/Excluir_Tipo_Pagamento.php?idtipo_pagamento=<?= $paymentType["idtipo_pagamento"] ?>">Excluir</a>
                        </td>
                    </tr>
                <?php   }  ?>
            </tbody>
    </table>
</div>
<br><br>

<div class="center1 font2">Lista de Reservas juntamente com Tipo de Pagamento utilizado</div>
    <div style="overflow: auto; width: 640px; height: 200px; border:solid 1px">
        <table class="tabela" id="sql" style="width:800px; text-align:center;">
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Valor</th>
                    <th>Data - Venda</th>
                    <th>Tipo de Pagamento</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($reservas_e_tipoPagamento as $reserva_com_tipoPagamento) { ?>
                    <tr>
                        <td style="text-align: center;"><?= $reserva_com_tipoPagamento["codigo"] ?></td>
                        <td style="text-align: center;"><?= $reserva_com_tipoPagamento["valor"] ?></td>
                        <td style="text-align: center;"><?= $reserva_com_tipoPagamento["data_venda"] ?></td>
                        <td style="text-align: center;"><?= $reserva_com_tipoPagamento["descricao"] ?></td>
                    </tr>
                <?php   }  ?>
            </tbody>
    </table>
</div>

</div>